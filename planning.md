* [x] Fork and clone the starter project from django-one-shot
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip
* [x] Install django
* [x] Install black
* [x] Install flake8
* [x] Install djlint
* [x] Deactivate your virtual environment
* [x] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
---
##feature 2
* [x] Create a Django project named brain_two so that the manage.py file is in the top directory
* [x] Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list
* [x] Run the migrations
* [x] Create a super user
* [x] Runserver
* [x] test feature
---
##feature 3
* [x] create a TodoList model in the todos Django app
  * [x] name 	string 	maximum length of 100 characters
  * [x] created_on 	date time 	should be automatically set to the time the todolist was created
  * [x] TodoList model should implicitly convert to a string with the __str__ method that is the value of the name property.
* [x] make migrations
* [x] migrate
---
##feature 4
* [x] Register the TodoList model with the admin so that you can see it in the Django admin site.
---
##feature 5
* [x] create a TodoItem model in the todos Django app.
  * [x] task 	string 	maximum length of 100 characters
  * [x] due_date 	date time 	should be optional
  * [x] is_completed 	boolean 	should default to False
  * [x] list 	foreign key 	should be related to the TodoList model and have a related name of "items". It should automatically delete if the to-do list is deleted. (This is a cascade.)
* [x] the TodoItem model should implicitly convert to a string with the __str__ method that is the value of the task property.
* [x] makemigrations
* [x] migrate
* [x] test
---
##feature 6
* [x] Register the TodoItem model with the admin so that you can see it in the Django admin site.
* [x] test
---
##feature 7
* [x] Create a view that will get all of the instances of the TodoList model and put them in the context for the template.
* [x] Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
* [x] Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
* [x] Create a template for the list view that complies with the following specifications.
* [x] test
---
##feature 8
* [x] Create a view that shows the details of a particular to-do list, including its tasks
* [x] In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"
* [x] Create a template to show the details of the todolist and a table of its to-do items
* [x] Update the list template to show the number of to-do items for a to-do list
* [x] Update the list template to have a link from the to-do list name to the detail view for that to-do list
* [x] test
---
##feature 9
* [x] Create a create view for the TodoList model that will show the name field in the form and handle the form submission to create a new TodoList
* [ ] If the to-do list is successfully created, it should redirect to the detail page for that to-do list
* [x] Register that view for the path "create/" in the todos urls.py and the name "todo_list_create"
* [x] Create an HTML template that shows the form to create a new TodoList (see the template specifications below)
* [x] Add a link to the list view for the TodoList that navigates to the new create view
* [ ] test
---
##feature 10
* [x] Create an update view for the TodoList model that will show the name. field in the form and handle the form submission to change an existing TodoList
* [ ] If the to-do list is successfully edited, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path /todos/<int:pk>/edit in the todos urls.py and the name "todo_list_update".
* [x] Create an HTML template that shows the form to edit a new TodoList (see the template specifications below).
* [x] Add a link to the list view for the TodoList that navigates to the new update view.
* [ ] test
---
##feature 11
* [x] Create a delete view for the TodoList model that will show a delete button and handle the form submission to delete an existing TodoList.
* [x] If the to-do list is successfully deleted, it should redirect to the to-do list list view.
* [x] Register that view for the path /todos/<int:pk>/delete in the todos urls.py and the name "todo_list_delete".
* [x] Create an HTML template that shows the form to delete a new TodoList (see the template specifications below).
* [x] Add a link to the detail view for the TodoList that navigates to the new delete view.
* [x] test
---
##feature 12
* [x] Create a create view for the TodoItem model that will show the task field in the form and handle the form submission to create a new TodoItem.
* [ ] If the to-do list is successfully created, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path "items/create/" in the todos urls.py and the name 
* [x] Create an HTML template that shows the form to create a new TodoItem (see the template specifications below)."todo_item_create".
* [x] Add a link to the list view for the TodoList that navigates to the new create view.
* [ ] test
---
##feature 13
* [x] Create an update view for the TodoItem model that will show the task field, the due date field, an is_completed checkbox, and a select list showing the to-do lists in the form and handle the form submission to change an existing TodoItem.
* [ ] If the to-do item is successfully edited, it should redirect to the detail page for the to-do list.
* [x] Register that view for the path /todos/items/<int:pk>/edit in the todos urls.py and the name "todo_item_update".
* [x] Create an HTML template that shows the form to edit a TodoItem (see the template specifications below).
* [x] Add a link to the list view for the TodoList that navigates to the new update view.
* [ ] test